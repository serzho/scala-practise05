package com.practise

import javax.faces.bean.{RequestScoped, ManagedBean, SessionScoped}
import scala.reflect.BeanProperty
import javax.ejb.EJB

@ManagedBean(name="carsIndexController")
@RequestScoped
class CarsIndexController {

  @EJB
  var carsRepositoryBean: CarsRepositoryBean = _

  def getCars:Array[Car] = carsRepositoryBean.all.toArray
}
