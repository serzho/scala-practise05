package com.practise {

import javax.persistence._
import scala.reflect.BeanProperty
import javax.faces.bean.{RequestScoped, SessionScoped, ManagedBean}

@ManagedBean
@RequestScoped
@Entity
@Table(name = "cars")
class Car {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @BeanProperty
  var id: Int = _

  @BeanProperty
  var model: String = _

  @BeanProperty
  var number: String = _

  @BeanProperty
  var year: Int = _

  override def toString = id + " " + model + " " + number
}
}

