package com.practise

import javax.faces.bean.{RequestScoped, ManagedProperty, ManagedBean}
import scala.reflect.BeanProperty
import javax.ejb.EJB

@ManagedBean(name="carsEditController")
@RequestScoped
class CarsEditController {

  @ManagedProperty(value="#{car}")
  @BeanProperty
  var car: Car = null

  @EJB(mappedName = "carsRepositoryBean")
  var carsRepository: CarsRepositoryBean = _

  def find(id: String): Car = {
    if (car.id == 0 && id != "")
      car = carsRepository.find(id.toInt).getOrElse(new Car)
    car
  }

  def update: String = {
    carsRepository.save(car)
    "index?faces-redirect=true"
  }
}
