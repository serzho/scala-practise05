package com.practise

import javax.faces.bean.{RequestScoped, ManagedProperty, SessionScoped, ManagedBean}
import scala.reflect.BeanProperty
import javax.ejb.EJB

@ManagedBean(name="carsNewController")
@RequestScoped
class CarsNewController {

  @BeanProperty
  @ManagedProperty(value="#{car}")
  var car: Car = new Car

  @EJB(mappedName="carsRepositoryBean")
  var carsRepositoryBean: CarsRepositoryBean = _

  def create: String = {
    carsRepositoryBean.save(car)
    "index?faces-redirect=true"
  }
}
